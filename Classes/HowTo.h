//
//  HowTo.h
//  OriginalGame
//
//  Created by Daiki Tamiya on 2015/05/12.
//
//

#ifndef __OriginalGame__HowTo__
#define __OriginalGame__HowTo__

#include <iostream>
#include "cocos2d.h"

class HowTo : public cocos2d::Layer
{
protected:
	void showTitle();
	virtual bool init() override;
	//タップイベント
	cocos2d::EventListenerTouchOneByOne *listener;
	void menuAction();
	void backMenuAction();
    cocos2d::Sprite* _howto;
    cocos2d::Sprite* _attention;
public:
	static cocos2d::Scene* createScene();
	CREATE_FUNC(HowTo);
};

#endif /* defined(__OriginalGame__HowTo__) */