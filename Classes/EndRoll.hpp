//
//  EndRoll.hpp
//  Sayu
//
//  Created by 田宮大暉 on 2015/12/12.
//
//

#ifndef EndRoll_hpp
#define EndRoll_hpp

#include <stdio.h>
#include "extensions/cocos-ext.h"
USING_NS_CC_EXT;
USING_NS_CC;
class EndRoll : public cocos2d::Layer
{
public:
    
    EndRoll();
    virtual bool init();
    void backHomeAction();
    void showBackButton(float dt);
    void afterSomeTimes();
    static cocos2d::Scene* createScene();
    virtual ~EndRoll();
    CREATE_FUNC(EndRoll);
};

#endif /* EndRoll_hpp */
