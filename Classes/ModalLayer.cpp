//
//  ModalLayer.cpp
//  OriginalGame
//
//  Created by Daiki Tamiya on 2015/05/17.
//
//

#include "ModalLayer.h"
#include "GameScene.h"
#define kModalLayerPriority -1


USING_NS_CC;

bool ModalLayer::init()
{
	if ( !LayerColor::initWithColor(Color4B::WHITE) )
	{
		return false;
	}
    auto director = Director::getInstance();
    auto winSize = director->getWinSize();
	//イベントを飲み込むかどうか
	listener = EventListenerTouchOneByOne::create();
	listener->setSwallowTouches(true);
	
	listener->onTouchBegan = CC_CALLBACK_2(ModalLayer::onTouchBegan, this);
	this->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);
	this->setOpacity(100);
	
	return true;
}
bool ModalLayer::onTouchBegan(Touch* touch, Event* event) {
	// can not touch on back layers
	return true;
}