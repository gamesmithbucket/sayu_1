//
//  Segment.h
//  RightOrLeft
//
//  Created by Daiki Tamiya on 2015/06/13.
//
//

#ifndef __RightOrLeft__Segment__
#define __RightOrLeft__Segment__

#include <stdio.h>
#include "cocos2d.h"
#include "Config.h"
USING_NS_CC;

class Segment : public cocos2d::Sprite
{
protected:
	cocos2d::Point _startPos;
	kSegment _segmentType;
	const char* getSegmentImageFileName(kSegment segmentType);
	void initStartPos();
	virtual bool initWithSegmentType(kSegment segmentType);
	
public:
	static Segment* createWithSegmentType(kSegment segmentType);
	void start(float duration);
	int getType();
};

#endif /* defined(__RightOrLeft__Segment__) */