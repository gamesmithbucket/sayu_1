//
//  Segment.cpp
//  RightOrLeft
//
//  Created by Daiki Tamiya on 2015/06/13.
//
//

#include "Segment.h"
#include "Config.h"
#include "GameScene.h"
USING_NS_CC;

Segment* Segment::createWithSegmentType(kSegment segmentType)
{
	Segment *pRet = new Segment();
	if (pRet && pRet->initWithSegmentType(segmentType))
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		CC_SAFE_DELETE(pRet);
		return NULL;
	}
}

bool Segment::initWithSegmentType(kSegment segmentType)
{
	if (!Sprite::initWithFile(getSegmentImageFileName(segmentType))){
		return false;
	}
	_segmentType = segmentType;
	initStartPos();
	return true;
}

//セグメントの種類取得
int Segment::getType()
{
	return _segmentType;
}

//スタート位置計算、
void Segment::initStartPos()
{
	auto director = Director::getInstance();
	auto size = director->getWinSize();
	srand((unsigned int)time(NULL));
	
	auto xnum = rand() % 3;
	int segmentXPos;
	switch(xnum)
	{
		case 0:segmentXPos = size.width / 6 * 1; //96
			break;
			
		case 1:segmentXPos = size.width / 6 * 3; //307
			break;
			
		case 2:segmentXPos = size.width / 6 * 5; //535
			break;
	}
	_startPos = Vec2(segmentXPos, 1136);
	setPosition(_startPos);
}

//スタート
void Segment::start(float duration)
{
	initStartPos();
	Vec2 curpos = getPosition();
	auto goalPos = Vec2(curpos.x, 0);
	auto fall = MoveTo::create(duration, goalPos);
	runAction(fall);
}

//セグメント取得
const char* Segment::getSegmentImageFileName(kSegment segmentType)
{
	switch (segmentType) {
		case RIGHT:
			return "rite.png";
			
		case LEFT:
			return "left.png";
			
		case REDRIGHT:
			return "rite_red.png";
			
		case REDLEFT:
			return "left_red.png";
			
		default:
			CCAssert(false, "invalid coinType");
			return "";
	}
}
