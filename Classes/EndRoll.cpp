//
//  EndRoll.cpp
//  Sayu
//
//  Created by 田宮大暉 on 2015/12/12.
//
//

#include "EndRoll.hpp"
#include "Config.h"
#include "TitleScene.h"

USING_NS_CC;

USING_NS_CC;
using namespace CocosDenshion;

Scene* EndRoll::createScene()
{
    auto scene = Scene::create();
    auto layer = EndRoll::create();
    scene->addChild(layer);
    return scene;
}

EndRoll::EndRoll()
{
    
}

EndRoll::~EndRoll()
{
    
}

void EndRoll::backHomeAction()
{
    auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
    audioEngine->playEffect(MP3("decision3"));
    Scene* pScene = TitleScene::createScene();
    TransitionFade* transition = TransitionFade::create(0.5f, pScene);
    Director::getInstance()->replaceScene(transition);
}

void EndRoll::afterSomeTimes()
{
    this->scheduleOnce(schedule_selector(EndRoll::showBackButton), 7);
}

void EndRoll::showBackButton(float d)
{
    auto director = Director::getInstance();
    auto winSize = director->getWinSize();

    auto backButton = MenuItemImage::create("back_red.png", "back_pink.png", CC_CALLBACK_0(EndRoll::backHomeAction, this));
    backButton->setPosition(Vec2(winSize.width / 12.0, winSize.height / 17.0));
    backButton->setScale(0.8);
    Menu* bmenu = Menu::create(backButton,NULL);
    bmenu->setPosition(Point::ZERO);
    addChild(bmenu);
}

bool EndRoll::init()
{
    auto director = Director::getInstance();
    auto winSize = director->getWinSize();
    
    auto string_title = "THANK YOU FOR PLAYING !!";
    auto label = Label::createWithSystemFont(string_title, "Arial Rounded MT Bold", 50);
    label->setDimensions(winSize.width,0);
    label->setPosition(Vec2(winSize.width / 2.0, -winSize.height));
    label->setHorizontalAlignment(TextHAlignment::CENTER);
    addChild(label);
    auto up = MoveTo::create(6, Vec2(winSize.width / 2.0, winSize.height / 2.0));
    label->runAction(up);
	UserDefault* e_userDef = UserDefault::getInstance();
	e_userDef->setBoolForKey("isEnd", 1);
    //バックボタン表示
    afterSomeTimes();
    return true;
}
