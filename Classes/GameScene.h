//
//  GameScene.h
//  OriginalGame
//
//  Created by Daiki Tamiya on 2015/04/30.
//
//

#ifndef __OriginalGame__GameScene__
#define __OriginalGame__GameScene__

#include "cocos2d.h"
#include "Config.h"
#include "Segment.h"
#include <stdio.h>
#include <random>
#include "SegmentLayer.h"

class GameScene :public cocos2d::Layer
{
public:
	static cocos2d::Scene* createScene(bool isOniMode);
	static const bool NORMAL = false;
	static const bool ONI = true;
	
protected:
	enum kTag
	{
		kTagBackground,
		kTagRight,
		kTagLeft,
		kTagCenter,
		kTagModalLayer,
		kTagSegment,
	};
	
	enum kZOrder
	{
		kZOrderBackground,
		kZOrderColorLayer,
		kZOrderSegmentLayer,
		kZOrderSegment,
		kZOrderButton,
		kZOrdergameOver,
		kZOrderpLabel,
		kZOrderCount,
	};
	
	static GameScene* create(bool isOniMode);
	GameScene(bool isOniMode);
	bool init() override;
	void update(float dt);
	void onEnterTransitionDidFinish() override;
	
	//背景
	cocos2d::Sprite* _background;
	
	void showStartCount();
	
	void startGameLogic();
	void stopGameLogic();
	
	//ボタン
	void setupChoices();
	void pushChoices(cocos2d::Ref* sender);
	void showStopButton();
	void menuStopCallback();
	void reshowButton();
	void menuResume();
	void showTitleButton();
	void menuTitleCallback();
	void showResetButton();
	void menuResetCallback();
	
	//ゲームオーバー
	bool checkGameOver();
	void showGameOver();
	void showMiss();
	
	//スコア(NOMAL)
	void setupScoreLabel();
	CC_SYNTHESIZE(cocos2d::Label *, _highScoreLabel, HighScoreLabel);
	CC_SYNTHESIZE(cocos2d::Label *, _scoreLabel, ScoreLabel);
    CC_SYNTHESIZE(int, _score, Score);
	int _highscore = 0;
    
    //スコア(ONI)
    CC_SYNTHESIZE(cocos2d::Label *, _oniHighScoreLabel, oniHighScoreLabel);
    CC_SYNTHESIZE(cocos2d::Label *, _oniScoreLabel, oniScoreLabel);
    CC_SYNTHESIZE(int, _oniScore, OniScore);
    int _oniHighScore = 0;
	
	//カウント
	SegmentLayer* _segmentLayer;
	cocos2d::EventListenerTouchOneByOne *_eventListener;
	std::vector<Segment*> _segments;
	
	void layerColor();
	LayerColor* _layer;
	
	void checkSpeedUp();
	void showSpeedUp();
	int _count = 0;
	
	// 鬼モード関連
	const bool _isOniMode;
	float getAddSegmentInterval();
	
    //エンドロール追加
    void showEndRoll();
    UserDefault* e_userDef = UserDefault::getInstance();
    
	void saveHighScore();
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event);
	void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *event);
};
#endif /* defined(__OriginalGame__GameScene__) */