//
//  GameScene.cpp
//  OriginalGame
//
//  Created by Daiki Tamiya on 2015/04/30.
//
//


#include "GameScene.h"
#include "ModalLayer.h"
#include "TitleScene.h"
#include "Config.h"
#include "Segment.h"
#include "SegmentLayer.h"
#include "EndRoll.hpp"
#include "SimpleAudioEngine.h"

USING_NS_CC;
using namespace std;

GameScene::GameScene(bool isOniMode)
:_score(0)
,_scoreLabel(NULL)
,_highScoreLabel(NULL)
,_oniScore(0)
,_oniScoreLabel(NULL)
,_oniHighScore(NULL)
,_isOniMode(isOniMode)
{
}

Scene* GameScene::createScene(bool isOniMode)
{
	auto scene = Scene::create();
	auto layer = GameScene::create(isOniMode);
	scene->addChild(layer);
	return scene;
}

GameScene* GameScene::create(bool isOniMode)
{
	GameScene *pRet = new GameScene(isOniMode);
	if (pRet && pRet->init())
	{
		pRet->autorelease();
		return pRet;
	}
	else
	{
		delete pRet;
		pRet = NULL;
		return NULL;
	}
}

void GameScene::showStartCount()
{
	auto s = Director::getInstance()->getWinSize();
	auto modal = ModalLayer::create();
	auto countLabel = Label::createWithSystemFont(to_string(3), "font/arial.ttf", 100);
	countLabel->setPosition(s.width*0.5f, s.height*0.5f);
	modal->addChild(countLabel);
	addChild(modal);
	
	auto rep = Repeat::create(Sequence::create(DelayTime::create(1),
											   CallFunc::create([=]{
		int count = atoi(countLabel->getString().c_str());
		
		if (count <= 1) {
			modal->removeFromParentAndCleanup(true);
			startGameLogic();
			auto start = Sprite::create("start.png");
			start->setPosition(s.width / 2.0, s.height / 2.0);
			start->setOpacity(150);
			start->setScale(0.5, 0.5);
			_background->addChild(start);
			auto large = ScaleTo::create(1, 1.5);
			auto remove = RemoveSelf::create(true);
			start->runAction(Sequence::create(large, remove, NULL));
		} else {
			auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
			audio->playEffect(MP3("cursor2"));
			countLabel->setString(to_string(--count));
		}
	}),NULL), 3);
	auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->playEffect(MP3("cursor2"));
	runAction(Sequence::create(rep, NULL));
}

float GameScene::getAddSegmentInterval()
{
	if (_isOniMode) {
		return 0.2f;
	} else {
        return 0.8f;
	}
}

// ゲームの動的処理の開始をここにいれていく感じで
void GameScene::startGameLogic()
{
	// 「update()」という関数を毎フレーム呼びます
	scheduleUpdate();//じめんとの衝突チェック→ぶつかってたらgameOver()を呼ぶ
	_segmentLayer->startGameLogic(getAddSegmentInterval());
}

// ゲームの動的処理の停止をここにいれていく感じで
void GameScene::stopGameLogic()
{
	unscheduleUpdate();
	_segmentLayer->stopSeg();
	_segmentLayer->pause();
}

void GameScene::pushChoices(cocos2d::Ref *sender)
{
	auto director = Director::getInstance();
	auto size = director->getWinSize();
	auto seg = _segmentLayer->getMinYSegment();
	// segmentがなかったら処理しない
	if (!seg) {
		return;
	}
	
	bool isCorrect;
	int buttonType = static_cast<Node*>(sender)->getTag();
	switch (buttonType) {
		case kTagRight:
			isCorrect = seg->getType() == RIGHT;
			break;
		case kTagLeft:
			isCorrect = seg->getType() == LEFT;
			break;
		case kTagCenter:
			isCorrect = (seg->getType() == REDLEFT || seg->getType() == REDRIGHT);
			break;
		default:
			CCASSERT(false, "incorrect button type");
			break;
	}
	
	if (!isCorrect) {
		showMiss();
	}else{
		const char* point;
		if(seg->getPositionY() > size.height / 2.0){
            if (_isOniMode){
                _oniScore += 2;
            }else{
                _score += 2;
            }
			point = "2P";
		}else{
            if (_isOniMode){
                _oniScore += 1;
            }else{
                _score += 1;
            }
			point = "1P";
		}
        if (_isOniMode) {
            _oniScoreLabel->setString(StringUtils::toString(_oniScore));
        }else{
            _scoreLabel->setString(StringUtils::toString(_score));
        }
		Label* pointLabelTwo = Label::createWithSystemFont(point, "Marker Felt", 100);
		pointLabelTwo->setColor(Color3B(0,128,0));
		pointLabelTwo->setOpacity(128);
		pointLabelTwo->setPosition(seg->getPosition());
		
		auto segPos = seg->getPosition();
		auto endPos = Vec2(segPos.x, segPos.y * 1.5);
		_segmentLayer->addChild(pointLabelTwo);
		auto up = MoveTo::create(0.5, endPos);
		auto remove = RemoveSelf::create(true);
		pointLabelTwo->runAction(Sequence::create(up, remove, NULL));
		
		// 作成したパーティクルのプロパティリストを読み込み
		auto particle = ParticleSystemQuad::create("particle_texture.plist");
		// パーティクルを開始
		particle->resetSystem();
		// パーティクルを表示する場所の設定
		particle->setPosition(segPos);
		// パーティクルを配置
		addChild(particle);
		
		CocosDenshion::SimpleAudioEngine::getInstance()->playEffect(MP3("shot1"));
		_segmentLayer->removeSegment(seg);
		checkSpeedUp();
        if (_isOniMode){
            e_userDef->setBoolForKey("isEnd", true);
            e_userDef->flush();
        }else{
            auto userDef = UserDefault::getInstance();
            auto highScore = userDef->getIntegerForKey("highscore");
            if (highScore >= 50){
                e_userDef->setBoolForKey("isEnd", true);
                e_userDef->flush();
            }else{
                if (_score >= 50){
                    e_userDef->setBoolForKey("isEnd", false);
                    e_userDef->flush();
                    showEndRoll();
                }else{
                    
                }
            }
        }
	}
}

//ミス
void GameScene::showMiss()
{
	auto director =Director::getInstance();
	auto winSize = director->getWinSize();
	_segmentLayer->miss();
	auto missLabel = Label::createWithSystemFont("Miss!", "Marker Felt",30);
	missLabel->setColor(Color3B(0, 0, 0));
	missLabel->setPosition(Vec2(winSize.width / 2, winSize.height / 2 - 80));
	missLabel->setScale(3.0);
	ModalLayer* layer = ModalLayer::create();
	_segmentLayer->addChild(layer, kZOrdergameOver, kTagModalLayer);
	layer->addChild(missLabel);
	saveHighScore();
	showTitleButton();
	showResetButton();
}

//停止ボタン生成
void GameScene::showStopButton()
{
	auto director =Director::getInstance();
	auto winSize = director->getWinSize();
	
	auto stopButton = MenuItemImage::create("stop_button_black.png", "stop_button_glay.png",CC_CALLBACK_0 (GameScene::menuStopCallback, this));
	//stopButton->setPosition(Vec2(winSize.width / 9.0 * 8.0, winSize.height / 9.0 * 8.0));
	stopButton->setScale(0.5);
	Menu* menu = Menu::create(stopButton,NULL);
	menu->setPosition(Vec2::ZERO);
	_background->addChild(menu, kZOrderpLabel);
}

//停止ボタンタップ時の処理
void GameScene::menuStopCallback()
{
	auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
	audioEngine->playEffect("decision3");
	stopGameLogic();
	reshowButton();
	showTitleButton();
	showResetButton();
}

//停止解除ボタン生成
void GameScene::reshowButton()
{
	auto director = Director::getInstance();
	auto size = director->getWinSize();
	
	ModalLayer* modal = ModalLayer::create();
	//modal->setPosition(Vec2(0, size.height / 30));
	MenuItemImage* restartButton = MenuItemImage::create("play_button_black.png", "play_button_glay.png", CC_CALLBACK_0(GameScene::menuResume, this));//リセットボタンの画像生成
	restartButton->setPosition(Vec2(size.width / 2, size.height / 2));
	Menu* pMenu = Menu::create(restartButton, NULL);//pMenuを作る
	pMenu->setPosition(Vec2(size.width / 2, size.height / 2));
	pMenu->setPosition(Vec2::ZERO);
	_segmentLayer->addChild(modal, kZOrderpLabel, kTagModalLayer);
	modal->addChild(pMenu, kZOrderpLabel);
}

//停止解除ボタンタップ時の処理
void GameScene::menuResume()
{
	auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
	audioEngine->playEffect(MP3("decision3"));
	scheduleUpdate();
	_segmentLayer->resumeSeg();
	auto modal = _segmentLayer->getChildByTag(kTagModalLayer);
	modal->removeFromParentAndCleanup(true);
	_segmentLayer->resume();
}

//タイトルボタン作成
void GameScene::showTitleButton()
{
	auto director = Director::getInstance();
	auto winSize = director->getWinSize();
	auto titleButton = MenuItemImage::create("home_red.png", "home_black.png",CC_CALLBACK_0(GameScene::menuTitleCallback, this));
	titleButton->setPosition(Vec2(winSize.width / 1.5, winSize.height / 4));
	Menu* titleMenu = Menu::create(titleButton,NULL);
	titleMenu->setPosition(Vec2::ZERO);
	auto modal = _segmentLayer->getChildByTag(kTagModalLayer);
	modal->addChild(titleMenu);
}

//タイトルボタンタップ時の処理
void GameScene::menuTitleCallback(){
	auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
	audioEngine->playEffect(MP3("decision3"));
	auto modal = _segmentLayer->getChildByTag(kTagModalLayer);
	modal->removeFromParentAndCleanup(true);
	Scene* pScene = TitleScene::createScene();
	TransitionFlipX* transition = TransitionFlipX::create(0.5f, pScene);
	Director::getInstance()->replaceScene(transition);
}

//リセットボタン生成
void GameScene::showResetButton()
{
	auto director =Director::getInstance();
	auto winSize = director->getWinSize();
	auto resetButton = MenuItemImage::create("Reset_blue.png", "Reset_dark.png",CC_CALLBACK_0(GameScene::menuResetCallback, this));
	resetButton->setPosition(Vec2(winSize.width / 3.5, winSize.height / 4));
	Menu* resetMenu = Menu::create(resetButton,NULL);
	resetMenu->setPosition(Vec2::ZERO);
	auto modal = _segmentLayer->getChildByTag(kTagModalLayer);
	modal->addChild(resetMenu);
}

//リセットボタンタップ時の動作
void GameScene::menuResetCallback(){
	auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
	audioEngine->playEffect(MP3("decision3"));
	ModalLayer* layer = (ModalLayer*)_segmentLayer->getChildByTag(kTagModalLayer);
	layer->removeFromParentAndCleanup(true);
	Scene* pScene = GameScene::createScene(_isOniMode);
	TransitionFlipX* transition = TransitionFlipX::create(0.5f, pScene);
	Director::getInstance()->replaceScene(transition);
}

//ゲームオーバー
bool GameScene::checkGameOver()
{
	auto seg = _segmentLayer->getMinYSegment();
	// segmentがなかったら処理しない
	if (!seg) {
		return false;
	}
	if (seg->getPositionY() <= 0) {
		return true;
	}
	
	return false;
}

void GameScene::showGameOver()
{
	auto size = Director::getInstance()->getWinSize();
	auto gameOverLabel = Label::createWithSystemFont("GAME OVER!", "Marker Felt", 30);
	gameOverLabel->setColor(Color3B(0,0,0));
	gameOverLabel->setPosition(Vec2(size.width / 2, size.height / 2 - 80));
	gameOverLabel->setScale(3.0);
	ModalLayer* layer = ModalLayer::create();
	_segmentLayer->addChild(layer, kZOrdergameOver, kTagModalLayer);
	layer->addChild(gameOverLabel);
	_segmentLayer->gameOver();
	saveHighScore();
	
	unscheduleUpdate();
	showTitleButton();
	showResetButton();
}

//ハイスコア保存
void GameScene::saveHighScore()
{
	auto director = Director::getInstance();
	auto winSize = director->getWinSize();
    if (_isOniMode){
        CCLOG("ONI");
        auto oniUserDef = UserDefault::getInstance();
        auto oniOldHighScore = oniUserDef->getIntegerForKey("oniHighScore", 0);
        if(oniOldHighScore < _oniScore){
            _oniHighScore = _oniScore;
            oniUserDef->setIntegerForKey("oniHighScore", _oniScore);
            oniUserDef->flush();
            auto newOniRecordlabel = Label::createWithSystemFont("NEW RECORD", "arial", 70);
            auto newOniRecord = String::createWithFormat("NEW RECORD %d",_oniHighScore);
            newOniRecordlabel->setScale(0.5);
            auto action = ScaleBy::create(1, 2);
            newOniRecordlabel->setString(newOniRecord->getCString());
            newOniRecordlabel->setColor(Color3B(255, 0, 0));
            newOniRecordlabel->setPosition(Vec2(winSize.width / 2,winSize.height / 2 + 250));
            ModalLayer* modal = (ModalLayer*)_segmentLayer->getChildByTag(kTagModalLayer);
            modal->addChild(newOniRecordlabel);
            newOniRecordlabel->runAction(action);
            CCLOG("score:%d, oldONIHighScore:%d", _oniScore, oniOldHighScore);
        }
    }else{
        CCLOG("NOMAL");
        auto userDef = UserDefault::getInstance();
        auto oldHighScore = userDef->getIntegerForKey("highscore", 0);
        if(oldHighScore < _score){
            _highscore = _score;
            userDef->setIntegerForKey("highscore", _score);
            userDef->flush();
            auto newRecordlabel = Label::createWithSystemFont("NEW RECORD", "arial", 70);
            auto newRecord = String::createWithFormat("NEW RECORD %d",_score);
            newRecordlabel->setScale(0.5);
            auto action = ScaleBy::create(1, 2);
            newRecordlabel->setString(newRecord->getCString());
            newRecordlabel->setColor(Color3B(255, 0, 0));
            newRecordlabel->setPosition(Vec2(winSize.width / 2,winSize.height / 2 + 250));
            ModalLayer* modal = (ModalLayer*)_segmentLayer->getChildByTag(kTagModalLayer);
            modal->addChild(newRecordlabel);
            newRecordlabel->runAction(action);
            //CCLOG("highScore:%d, score:%d", _highscore, _score);
        }
        CCLOG("score:%d, oldHighScore:%d", _score, oldHighScore);
    }
	
}

//スピードアップチェック
void GameScene::checkSpeedUp()
{
    if(_isOniMode){
        
    }else{
        if ( ((_score == 10 || _score == 11) && _count == 0 ) || ((_score == 20 || _score == 21) && _count == 1 ) || ((_score == 30 || _score == 31) && _count == 2 ) )
        {
            _segmentLayer->accelerateIfNeeded(_score);
            showSpeedUp();
        }
    }
}

// スピードアップ演出
void GameScene::showSpeedUp()
{
	auto director = Director::getInstance();
	auto size = director->getWinSize();
	auto speedLabel = Label::createWithSystemFont("SPEED UP", "Marker Felt", 30);
	speedLabel->enableOutline(Color4B::BLACK, 1.5);
	speedLabel->setColor(Color3B(255, 255, 255));
	speedLabel->setOpacity(150);
	speedLabel->setPosition(Vec2(size.width / 2.0, size.height / 2.0));
	_segmentLayer->addChild(speedLabel, kZOrderpLabel);
	auto large = ScaleTo::create(1, 4.0);
	auto remove = RemoveSelf::create(true);
	speedLabel->runAction(Sequence::create(large, remove, NULL));
	_count += 1;
}

void GameScene::update(float dt)
{
	if (_score >= 50) {
		showEndRoll();
	}
}

//タップイベント
bool GameScene::onTouchBegan(cocos2d::Touch *ptouch, cocos2d::Event* pevent)
{
	return true;
}

void GameScene::onTouchMoved(cocos2d::Touch *ptouch, cocos2d::Event* pevent)
{
	
}

void GameScene::onTouchEnded(cocos2d::Touch *ptouch, cocos2d::Event *pevent)
{
	
}

void GameScene::onTouchCancelled(cocos2d::Touch *ptouch, cocos2d::Event *pevent)
{
	//タッチが終わった時の処理
}

void GameScene::onEnterTransitionDidFinish()
{
	showStartCount();
}

#pragma mark - ゲーム起動時だけ系

bool GameScene::init()
{
	// ゲームの準備
	if(!Layer::init()){
		return false;
	}
	UserDefault* e_userDef = UserDefault::getInstance();
	e_userDef->setBoolForKey("isEnd", 0);
	srand((unsigned) time(NULL));
	auto s = Director::getInstance()->getWinSize();
	
	// 高得点ゾーン
	_layer = LayerColor::create(Color4B(0, 255, 0, 50), s.width, s.height / 2);
	_layer->setPosition(Vec2(0, s.height / 2));
	addChild(_layer, kZOrderColorLayer);
	
	// セグメントレイヤー
	_segmentLayer = SegmentLayer::create();
	addChild(_segmentLayer, kZOrderSegmentLayer);
	
	// 背景
	_background = Sprite::create("background.png");
	_background->setPosition(s.width / 2.0, s.height / 2.0);
	addChild(_background, kZOrderBackground);
	
	setupChoices();
	setupScoreLabel();
	
	// 停止ボタン
	auto stopButton = MenuItemImage::create("stop_button_black.png", "stop_button_glay.png",CC_CALLBACK_0 (GameScene::menuStopCallback, this));
	stopButton->setPosition(Vec2(s.width / 9.0 * 8.0, s.height / 9.0 * 7.8));
	stopButton->setScale(0.5);
	auto menu = Menu::create(stopButton,NULL);
	menu->setPosition(Vec2::ZERO);
	_background->addChild(menu, kZOrderpLabel);
	
	
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	_eventListener = EventListenerTouchOneByOne::create();
	_eventListener->onTouchBegan		= CC_CALLBACK_2(GameScene::onTouchBegan, this);
	_eventListener->onTouchMoved		= CC_CALLBACK_2(GameScene::onTouchMoved, this);
	_eventListener->onTouchEnded		= CC_CALLBACK_2(GameScene::onTouchEnded, this);
	_eventListener->onTouchCancelled	= CC_CALLBACK_2(GameScene::onTouchCancelled, this);
	dispatcher->addEventListenerWithSceneGraphPriority(_eventListener, this);
    
    return true;
}

void GameScene::setupChoices()
{
	auto s = Director::getInstance()->getWinSize();
	auto rItem = MenuItemImage::create("Right_black.png", "Right_glay.png", CC_CALLBACK_1(GameScene::pushChoices, this));
	rItem->setPosition(s.width / 6.0 * 5.0, s.height / 10.0);
    rItem->setScale(1.5);
	rItem->setTag(kTagRight);
	auto lItem = MenuItemImage::create("Left_black.png", "Left_glay.png", CC_CALLBACK_1(GameScene::pushChoices, this));
	lItem->setPosition(s.width / 6.0, s.height / 10.0);
    lItem->setScale(1.5);
	lItem->setTag(kTagLeft);
	auto cItem = MenuItemImage::create("center_red.png", "center_pink.png", CC_CALLBACK_1(GameScene::pushChoices, this));
	cItem->setPosition(s.width / 2.0, s.height / 10.0);
    cItem->setScale(1.5);
	cItem->setTag(kTagCenter);
	
	auto menu = Menu::create(rItem, lItem, cItem, NULL);
	menu->setPosition(Vec2::ZERO);
	_background->addChild(menu, kZOrderButton);
}

void GameScene::setupScoreLabel()
{
	//Directorを取り出す
	auto director = Director::getInstance();
	//画面サイズを取り出す
	auto size = director->getWinSize();
	
    if (_isOniMode){
        // ONIスコア
        _oniScoreLabel = Label::createWithSystemFont(StringUtils::toString(_oniScore), "Marker Felt", 30);
        _oniScoreLabel->setPosition(Vec2(size.width / 7.0, size.height / 1.23));
        _oniScoreLabel->enableShadow();//影をつける
        _oniScoreLabel->enableOutline(Color4B::RED, 1.5);
        _segmentLayer->addChild(_oniScoreLabel, kZOrderpLabel);
        
        // ONIスコアヘッダー
        auto oniScoreLabelHeader = Label::createWithSystemFont("SCORE", "Marker Felt", 30);
        oniScoreLabelHeader->enableShadow(Color4B::BLACK, Size(0.5, 0.5), 3);
        oniScoreLabelHeader->enableOutline(Color4B::BLACK, 1.5);
        oniScoreLabelHeader->setPosition(Vec2(size.width / 7.0, size.height / 1.2));
        _segmentLayer->addChild(oniScoreLabelHeader, kZOrderpLabel);
        
        // ONIハイスコア
        auto oniHighScore = UserDefault::getInstance()->getIntegerForKey("oniHighScore", 0);
        _oniHighScoreLabel = Label::createWithSystemFont(StringUtils::toString(oniHighScore), "Marker Felt", 30);
        _oniHighScoreLabel->setPosition(Vec2(size.width / 7.0, size.height / 1.15));
        _oniHighScoreLabel->setColor(Color3B(255, 215, 0));
        _oniHighScoreLabel->enableOutline(Color4B::BLACK, 1.5);
        _segmentLayer->addChild(_oniHighScoreLabel, kZOrderpLabel);
        
        // ONIハイスコアヘッダー
        auto oniHighScoreLabelHeader = Label::createWithSystemFont("ONI HIGH SCORE", "Marker Felt", 28);
        oniHighScoreLabelHeader->setColor(Color3B(255, 0, 255));
        oniHighScoreLabelHeader->setPosition(Vec2(size.width / 7.0, size.height / 1.12));
        _segmentLayer->addChild(oniHighScoreLabelHeader, kZOrderpLabel);
    }else{
        // スコア
        _scoreLabel = Label::createWithSystemFont(StringUtils::toString(_score), "Marker Felt", 30);
        _scoreLabel->setPosition(Vec2(size.width / 7.0, size.height / 1.23));
        _scoreLabel->enableShadow();//影をつける
        _scoreLabel->enableOutline(Color4B::RED, 1.5);
        _segmentLayer->addChild(_scoreLabel, kZOrderpLabel);
        
        //スコアヘッダー
        auto scoreLabelHeader = Label::createWithSystemFont("SCORE", "Marker Felt", 30);
        scoreLabelHeader->enableShadow(Color4B::BLACK, Size(0.5, 0.5), 3);
        scoreLabelHeader->enableOutline(Color4B::BLACK, 1.5);
        scoreLabelHeader->setPosition(Vec2(size.width / 7.0, size.height / 1.2));
        _segmentLayer->addChild(scoreLabelHeader, kZOrderpLabel);
        
        // ハイスコア
        auto highScore = UserDefault::getInstance()->getIntegerForKey("highscore", 0);
        _highScoreLabel = Label::createWithSystemFont(StringUtils::toString(highScore), "Marker Felt", 30);
        _highScoreLabel->setPosition(Vec2(size.width / 7.0, size.height / 1.15));
        _highScoreLabel->setColor(Color3B(255, 215, 0));
        _highScoreLabel->enableOutline(Color4B::BLACK, 1.5);
        _segmentLayer->addChild(_highScoreLabel, kZOrderpLabel);
	
        // ハイスコアヘッダー
        auto highScoreLabelHeader = Label::createWithSystemFont("HIGH SCORE", "Marker Felt", 30);
        highScoreLabelHeader->enableShadow(Color4B::BLACK, Size(0.5, 0.5), 3);
        highScoreLabelHeader->setColor(Color3B(255, 215, 0));
        highScoreLabelHeader->enableOutline(Color4B::YELLOW, 1.5);
        highScoreLabelHeader->setPosition(Vec2(size.width / 7.0, size.height / 1.12));
        _segmentLayer->addChild(highScoreLabelHeader, kZOrderpLabel);
    }
}

//EndRoll表示
void GameScene::showEndRoll()
{
	
	e_userDef->setBoolForKey("isEnd", true);
	e_userDef->flush();
	Scene* endScene = EndRoll::createScene();
	TransitionFade* transition = TransitionFade::create(0.5f, endScene);
	 Director::getInstance()->replaceScene(transition);
}
