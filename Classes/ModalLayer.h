//
//  ModalLayer.h
//  OriginalGame
//
//  Created by Daiki Tamiya on 2015/05/17.
//
//

#ifndef __OriginalGame__ModalLayer__
#define __OriginalGame__ModalLayer__
#include <iostream>
#include "cocos2d.h"

USING_NS_CC;

class ModalLayer : public cocos2d::LayerColor
{
private:
	cocos2d::EventListenerTouchOneByOne* listener;
	
public:
	virtual bool init();
	void menuCloseCallback(Ref* pSender);
	virtual bool onTouchBegan(cocos2d::Touch *pTouch, cocos2d::Event *pEvent);
	CREATE_FUNC(ModalLayer);
};
#endif /* defined(__OriginalGame__ModalLayer__) */