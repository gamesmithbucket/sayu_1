//
//  HowTo.cpp
//  OriginalGame
//
//  Created by Daiki Tamiya on 2015/05/12.
//
//

#include "TitleScene.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "HowTo.h"
#include "Config.h"


USING_NS_CC;
using namespace CocosDenshion;

Scene* HowTo::createScene()
{
	auto scene = Scene::create();
	auto layer = HowTo::create();
	scene->addChild(layer);
	return scene;
}

void HowTo::menuAction()
{
	auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
	audioEngine->playEffect(MP3("decision3"));
	Scene* pScene = GameScene::createScene(GameScene::NORMAL);
	TransitionTurnOffTiles* transition = TransitionTurnOffTiles::create(0.5f, pScene);
	Director::getInstance()->replaceScene(transition);
}

void HowTo::backMenuAction()
{
    auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
    audioEngine->playEffect(MP3("decision3"));
	Scene* pScene = TitleScene::createScene();
	TransitionTurnOffTiles* transition = TransitionTurnOffTiles::create(0.5f, pScene);
	Director::getInstance()->replaceScene(transition);
}

bool HowTo::init()
{
    
	if(!Layer::init()){
		return false;
	}
	
	auto director = Director::getInstance();
	auto winSize = director->getWinSize();
	
	//背景追加
	_howto = Sprite::create("HowTo.png");
	_howto->setPosition(Vec2(winSize.width / 2.0, winSize.height / 2.0));
	this->addChild(_howto);
	
	//ゴーボタン表示
	auto goButton = MenuItemImage::create("go_red.png", "go_pink.png", CC_CALLBACK_0(HowTo::menuAction, this));
	goButton->setPosition(Vec2(winSize.width * 0.3 , winSize.height / 12.0));
	auto large = ScaleTo::create(0.5f, 1.2);
	auto small = ScaleTo::create(0.1f, 0.83);
	auto spawn = Spawn::create(large, small, NULL);
	auto repeatForever = RepeatForever::create(spawn);
	goButton->runAction(repeatForever);
	Menu* gmenu = Menu::create(goButton,NULL);
	gmenu->setPosition(Point::ZERO);
	_howto->addChild(gmenu);
	
	// 鬼モード
    UserDefault* e_userDef = UserDefault::getInstance();
    bool isEnd = e_userDef->getBoolForKey("isEnd");
//	e_userDef->setBoolForKey("isEnd", 0);
	CCLOG("isEnd=%d", isEnd);
    if (isEnd){
        auto oniLabel = Label::createWithSystemFont("Go ONI Mode", "font/arial.ttf", 40);
        oniLabel->setTextColor(Color4B(200, 0, 255, 255));
        auto oniItem = MenuItemLabel::create(oniLabel, [](Ref* sender){
		auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
		audioEngine->playEffect(MP3("decision3"));
		Scene* pScene = GameScene::createScene(GameScene::ONI);
		Director::getInstance()->replaceScene(TransitionFade::create(0.5f, pScene, Color3B::RED));
        });
        oniItem->setPosition(winSize.width * 0.7, winSize.height / 12.0);
        gmenu->addChild(oniItem);
    }else{
        auto oniLabel = Label::createWithSystemFont("Go ONI Mode", "font/arial.ttf", 40);
        oniLabel->setTextColor(Color4B(200, 0, 255, 255));
        oniLabel->setOpacity(50);
        oniLabel->setPosition(winSize.width * 0.7, winSize.height / 12.0);
        _howto->addChild(oniLabel);
    }
		
	//バックボタン表示
	auto backButton = MenuItemImage::create("back_red.png", "back_pink.png", CC_CALLBACK_0(HowTo::backMenuAction, this));
	backButton->setPosition(Vec2(winSize.width / 12.0, winSize.height / 17.0));
	backButton->setScale(0.8);
	Menu* bmenu = Menu::create(backButton,NULL);
	bmenu->setPosition(Point::ZERO);
	_howto->addChild(bmenu);
	
	return true;
}
