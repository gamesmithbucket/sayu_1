//
//  Config.h
//  RightOrLeft
//
//  Created by Daiki Tamiya on 2015/06/13.
//
//

#ifndef RightOrLeft_Config_h
#define RightOrLeft_Config_h


using namespace std;

enum kSegment
{
	RIGHT,
	LEFT,
	REDRIGHT,
	REDLEFT,
};

enum kButton
{
	kRIGHT,
	kLEFT,
	kCENTER,
};


static const char* MP3(const char* name)
{
	return (string("sound/") + string(name) + string(".mp3")).c_str();
}
#endif