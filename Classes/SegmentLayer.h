//
//  SegmentLayer.h
//  RightOrLeft
//
//  Created by Daiki Tamiya on 2015/07/03.
//
//

#ifndef __RightOrLeft__SegmentLayer__
#define __RightOrLeft__SegmentLayer__

#include <stdio.h>
#include "cocos2d.h"
#include "Config.h"
#include "Segment.h"
#include <random>

class SegmentLayer :public cocos2d::Layer
{
protected:
	
	enum kTag
	{
		kTagSegment,
	};
	
	enum kZOrder
	{
		kZOrderSegment,
		kZOrdergameOver,
	};
	bool init() override;
	std::vector<Segment*> _segments;
	cocos2d::EventListenerTouchOneByOne *eventListener;
	SegmentLayer* _segmentLayer;
	//void update(float dt);
	kSegment _segmentType;
	
	
public:
	// 加速関連
	float _segmentDuration = 1.5f;
	void accelerateIfNeeded(int score);
	
	void startGameLogic(float interval);
	
	void removeSegment(Segment* seg);
	void gameOver();
	void miss();
	void addSegment(float dt);
	void stopSeg();
	void resumeSeg();
	Segment* getMinYSegment();
	bool onTouchBegan(cocos2d::Touch *touch, cocos2d::Event *event){return true;};
	void onTouchMoved(cocos2d::Touch *touch, cocos2d::Event *event){};
	void onTouchEnded(cocos2d::Touch *touch, cocos2d::Event *event){};
	void onTouchCancelled(cocos2d::Touch *touch, cocos2d::Event *event){};
	CREATE_FUNC(SegmentLayer);
};

#endif /* defined(__RightOrLeft__SegmentLayer__) */