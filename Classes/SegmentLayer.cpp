//
//  SegmentLayer.cpp
//  RightOrLeft
//
//  Created by Daiki Tamiya on 2015/07/03.
//
//

#include "SegmentLayer.h"
#include "ModalLayer.h"
#include "TitleScene.h"
#include "Config.h"
#include "Segment.h"

USING_NS_CC;

void SegmentLayer::startGameLogic(float interval)
{
	// 「update()」という関数を毎フレーム呼びます
	schedule(schedule_selector(SegmentLayer::addSegment), interval);
}


//segment表示
void SegmentLayer::addSegment(float dt)
{
	auto director = Director::getInstance();
	auto size = director->getWinSize();
	auto Probability = rand() % 100;
	if (Probability >= 0 && Probability < 16){
		_segmentType = REDRIGHT;
	}else if(Probability >= 16 && Probability < 31){
		_segmentType = REDLEFT;
	}else if(Probability >= 31 && Probability < 66){
		_segmentType = RIGHT;
	}else{
		_segmentType = LEFT;
	}
    Segment* newSegment = Segment::createWithSegmentType(_segmentType);
    auto advent = rand() % 100;
    if (advent >= 0 && advent < 34){
        newSegment->setPosition(Vec2((size.width / 6), size.height));
    }else if (advent >= 34 && advent < 67){
        newSegment->setPosition(Vec2((size.width / 6 * 3), size.height));
    }else{
        newSegment->setPosition(Vec2((size.width / 6 * 5), size.height));
    }
	addChild(newSegment, kZOrderSegment, kTagSegment);
	auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->playEffect(MP3("ta_ta_syun01"));
	newSegment->start(_segmentDuration);
	_segments.push_back(newSegment);
}

// 加速する
void SegmentLayer::accelerateIfNeeded(int score)
{
	if (score < 10 && _segmentDuration != 1.5f){
		_segmentDuration = 1.5f;
	}else if (score >= 10 && score < 20 && _segmentDuration != 1.2f){
		_segmentDuration = 1.3f;
	}else if (score >= 20 && score < 30 && _segmentDuration != 0.9f){
		_segmentDuration = 1.1f;
	}else if (score >= 30 && score < 40 && _segmentDuration != 0.7f){
		_segmentDuration = 0.9f;
	}else if ( _segmentDuration != 0.5f) {
		_segmentDuration = 0.7f;
	}
}

//最下位のsegmentを返す
Segment* SegmentLayer::getMinYSegment()
{
	// segmentが１つもなかったらnullを返す
	Segment* minYSegment = nullptr;
	float minY = Director::getInstance()->getWinSize().height;
	// segmentが１つでもあるときだけ
	if (_segments.size()) {
		for (auto segment : _segments) {
			float segY = segment->getPositionY();
			if (segY < minY) {
				minY = segY;
				minYSegment = segment;
			}
		}
	}
	return minYSegment;
}

//停止処理
void SegmentLayer::stopSeg()
{
	Vector<Segment*>::iterator it;
	for(it = _segments.begin(); it != _segments.end(); it++){
		((Segment*)*it)->pause();
	}
}

//動く処理
void SegmentLayer::resumeSeg()
{
	cocos2d::Vector<Segment*>::iterator it2;
	for(auto it2 = _segments.begin(); it2 != _segments.end(); ++it2)
	{
		((Segment*)*it2)->resume();
	}
}

//削除処理
void SegmentLayer::removeSegment(Segment* seg)
{
	// 全セグメントに対して
	for (auto it = _segments.begin(); it != _segments.end(); ++it) {
		// ループで回ってきたitが消したいsegment(引数のsegment)のときだけ
		if (*it == seg) {
			_segments.erase(it);
			// １回の関数呼び出しで消したいsegmentは１つ
			break;
		}
	}
	// cocos2d-xのノード関係からも消す（表示も消える）
	seg->removeFromParentAndCleanup(true);
}

//ゲームオーバー
void SegmentLayer::gameOver()
{
	auto director = Director::getInstance();
	auto size = director->getWinSize();
	auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->playEffect(MP3("sen_ge_hasai02"));
	
	// 作成したパーティクルのプロパティリストを読み込み
	ParticleSystemQuad* particle = ParticleSystemQuad::create("particle_burn.plist");
	
	// パーティクルを開始
	particle->resetSystem();
	
	// パーティクルを表示する場所の設定
	particle->setPosition(Vec2(size.width / 2.0, 0));
	
	// パーティクルを配置
	this->addChild(particle);
	unscheduleAllSelectors();
	stopAllActions();
	stopSeg();
}

void SegmentLayer::miss()
{
	auto director = Director::getInstance();
	auto size = director->getWinSize();
	auto audio = CocosDenshion::SimpleAudioEngine::getInstance();
	audio->playEffect(MP3("sen_ge_hasai02"));
	// 作成したパーティクルのプロパティリストを読み込み
	ParticleSystemQuad* particle = ParticleSystemQuad::create("particle_burn.plist");
	
	// パーティクルを開始
	particle->resetSystem();
	
	// パーティクルを表示する場所の設定
	particle->setPosition(Vec2(size.width / 2.0, 0));
	
	// パーティクルを配置
	this->addChild(particle);
	unscheduleAllSelectors();
	stopAllActions();
	stopSeg();
}

//void SegmentLayer::update(float dt)
//{
//	//showGameOver();
//}

bool SegmentLayer::init()
{
	if(!Layer::init()){
		return false;
	}
	
	auto dispatcher = Director::getInstance()->getEventDispatcher();
	eventListener = EventListenerTouchOneByOne::create();
	eventListener->onTouchBegan		= CC_CALLBACK_2(SegmentLayer::onTouchBegan, this);
	eventListener->onTouchMoved		= CC_CALLBACK_2(SegmentLayer::onTouchMoved, this);
	eventListener->onTouchEnded		= CC_CALLBACK_2(SegmentLayer::onTouchEnded, this);
	eventListener->onTouchCancelled	= CC_CALLBACK_2(SegmentLayer::onTouchCancelled, this);
	dispatcher->addEventListenerWithSceneGraphPriority(eventListener, this);
	// scheduleUpdate();
	srand((unsigned) time(NULL));
	
	return true;
}