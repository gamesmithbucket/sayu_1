//
//  TitleScene.h
//  OriginalGame
//
//  Created by Daiki Tamiya on 2015/04/30.
//
//

#ifndef __OriginalGame__TitleScene__
#define __OriginalGame__TitleScene__

#include "cocos2d.h"

class TitleScene : public cocos2d::Layer
{
protected:
	TitleScene();
	void showTitle();
	virtual ~TitleScene();
	bool init() override;
	void showStartButton();
	//タップイベント
	void menuAction();
	void showTitleScene();
	//cocos2d::Sprite* _title;
public:
	static cocos2d::Scene* createScene();
	CC_SYNTHESIZE_RETAIN(cocos2d::Label *, _highScoreLabel, HighScoreLabel);
	//void onEnterTransitionDidFinish() override;
	CREATE_FUNC(TitleScene);
};

#endif /* defined(__OriginalGame__TitleScene__) */