//
//  TitleScene.cpp
//  OriginalGame
//
//  Created by Daiki Tamiya on 2015/04/30.
//
//

#include "TitleScene.h"
#include "GameScene.h"
#include "SimpleAudioEngine.h"
#include "HowTo.h"
//#include "Config.h"

USING_NS_CC;
using namespace CocosDenshion;

Scene* TitleScene::createScene()
{
	auto scene = Scene::create();
	auto layer = TitleScene::create();
	scene->addChild(layer);
	return scene;
}

TitleScene::TitleScene()
:_highScoreLabel(NULL)
{
	
}

TitleScene::~TitleScene()
{
	CC_SAFE_RELEASE_NULL(_highScoreLabel);
}

void TitleScene::menuAction()
{
	auto audioEngine = CocosDenshion::SimpleAudioEngine::getInstance();
	audioEngine->playEffect(MP3("decision3"));
	Scene* pScene = HowTo::createScene();
	TransitionFade* transition = TransitionFade::create(0.5f, pScene);
	Director::getInstance()->replaceScene(transition);
}

bool TitleScene::init()
{
	if(!Layer::init()){
		return false;
	}
	
	auto director = Director::getInstance();
	auto winSize = director->getWinSize();
	
	//背景追加
	auto background = Sprite::create("Right_and_Left.png");
	background->setPosition(Vec2(winSize.width / 2.0, winSize.height / 2.0));
	this->addChild(background);
	
	//ハイスコア表示(ノーマル)
	UserDefault* userDef = UserDefault::getInstance();
	userDef->setIntegerForKey("highscore", 0);
	auto highScore = userDef->getIntegerForKey("highscore", 0);
	auto highScoreLabel = Label::createWithSystemFont(StringUtils::toString(highScore), "Marker Felt", 30);
	this->setHighScoreLabel(highScoreLabel);
	highScoreLabel->setPosition(Vec2(winSize.width / 3.0 * 2.0, winSize.height / 2.8));
	highScoreLabel->setColor(Color3B(255, 215, 0));
	background->addChild(highScoreLabel);
	auto highScoreLabelHeader = Label::createWithSystemFont("HIGH SCORE:", "Marker Felt", 30);
	highScoreLabelHeader->setColor(Color3B(255, 215, 0));
	highScoreLabelHeader->setPosition(Vec2(winSize.width / 2.8, winSize.height / 2.8));
	background->addChild(highScoreLabelHeader);
    CCLOG("HighScore:%d", highScore);
	
	//ハイスコア表示(ONI)
    UserDefault* oniUserDef = UserDefault::getInstance();
    auto oniHighScore = oniUserDef->getIntegerForKey("oniHighScore", 0);
    auto oniHighScoreLabel = Label::createWithSystemFont(StringUtils::toString(oniHighScore), "Marker Felt", 30);
    this->setHighScoreLabel(oniHighScoreLabel);
    oniHighScoreLabel->setPosition(Vec2(winSize.width / 3.0 * 2.0, winSize.height / 3.3));
    oniHighScoreLabel->setColor(Color3B(255, 0, 255));
    background->addChild(oniHighScoreLabel);
    auto oniHighScoreLabelHeader = Label::createWithSystemFont("ONI HIGH SCORE:", "Marker Felt", 30);
    oniHighScoreLabelHeader->setColor(Color3B(255, 0, 255));
    oniHighScoreLabelHeader->setPosition(Vec2(winSize.width / 3.15, winSize.height / 3.3));
    background->addChild(oniHighScoreLabelHeader);
    CCLOG("ONIHighScore:%d", oniHighScore);
    
	//スタートボタン表示
	auto startButton = MenuItemImage::create("start_button.png", "start_button_red.png", CC_CALLBACK_0(TitleScene::menuAction, this));
	startButton->setPosition(Vec2(winSize.width / 2.0, winSize.height / 12.0));
	startButton->setScale(2.0);
	Menu* menu = Menu::create(startButton,NULL);
	menu->setPosition(Point::ZERO);
	this->addChild(menu);
	return true;
}

/*void TitleScene::onEnterTransitionDidFinish()
 {
 //BGMの再生
 auto audio = SimpleAudioEngine::getInstance();
 audio->preloadBackgroundMusic(MP3("waterblue"));
 audio->playBackgroundMusic(MP3("waterblue"), true);
 }*/
