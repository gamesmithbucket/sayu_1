//
//  AdMobHelper.m
//  Sayu
//
//  Created by 田宮大暉 on 2016/02/12.
//
//

#import <Foundation/Foundation.h>
#include "AdMobHelper.h"
#include "AppController.h"

@interface ViewController ()

@end

@implementation ViewController

#pragma mark -

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    // AdMobのインタースティシャル広告読み込み
    [self loadAdMobIntersBanner];
}

#pragma mark - AdMob Inters Banner

// AdMobのインタースティシャル広告読み込み
- (void)loadAdMobIntersBanner
{
    interstitial_ = [[GADInterstitial alloc] init];
    interstitial_.adUnitID = @"ca-app-pub-5282264971604058/4459983728";
    interstitial_.delegate = self;
    GADRequest *request = [GADRequest request];
    [interstitial_ loadRequest:request];
}

// AdMobのインタースティシャル広告表示
- (void)interstitialDidReceiveAd:(GADInterstitial *)ad
{
    [interstitial_ presentFromRootViewController:self];
}

@end