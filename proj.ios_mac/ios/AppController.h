#import <UIKit/UIKit.h>

@class RootViewController;

extern "C" {
#import <GoogleMobileAds/GoogleMobileAds.h>
}

@interface AppController : NSObject <UIApplicationDelegate> {
    UIWindow *window;
    GADBannerView *bannerView_; // 追加
}

@property(nonatomic, readonly) RootViewController* viewController;

@end

