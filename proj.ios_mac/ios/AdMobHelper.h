//
//  AdMobHelper.h
//  Sayu
//
//  Created by 田宮大暉 on 2016/02/12.
//
//

#ifndef AdMobHelper_h
#define AdMobHelper_h

#import <UIKit/UIKit.h>
#import <GoogleMobileAds/GADInterstitial.h>

@interface ViewController : UIViewController <GADInterstitialDelegate> {
    GADInterstitial *interstitial_;
}

@end
#endif /* AdMobHelper_h */
